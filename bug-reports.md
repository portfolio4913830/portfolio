# Баг-репорты

## Баг №1 При вводе корректных логина и пароля возникает ошибка, авторизация не успешна
*Предшествующие условия*:  
1. Открыто окно авторизации http://testingchallenges.thetestingmap.org/login/login.php    

*Серьезность* : **Blocker**  
*Шаги воспроизведения*:  
1. В поле "Username" ввести `admin`
2. В поле "Password" ввести `admin`
3. Нажать кнопку "Log In"  

*Ожидаемый результат*: В форме авторизации при вводе корректных авторизационных данных выводится сообщение `Logged In Successfully`, авторизация успешна   
*Фактический результат*: В форме авторизации при вводе корректных авторизационных данных выводится сообщение `Username Not Found Log In Successfull`, авторизация не успешна  
*Окружение* : Google Chrome, Версия 113.0.5672.93 

## Баг №2 При вводе корректных данных для авторизации меняется регистр логина (Первая буква логина всегда Uppercase)
*Предшествующие условия*:  
1. Открыто окно авторизации http://testingchallenges.thetestingmap.org/login/login.php    

*Серьезность* : **Blocker**  
*Шаги воспроизведения*: 
1. В поле "Username" ввести `admin`   

*Ожидаемый результат*: в введенном логине `admin` все буквы lowercase  
*Фактический результат*: в введенном логине `Admin` первая буква Uppercase   
*Окружение* : Google Chrome, Версия 113.0.5672.93  

## Баг №3 При попытке авторизации с одним заполненным полем выводится некорректная ошибка (несоответствие ТЗ) 
*Предшествующие условия*:  
1. Открыто окно авторизации http://testingchallenges.thetestingmap.org/login/login.php    

*Серьезность* : **Trivial**  
*Шаги воспроизведения*:  
1. В поле "Username" ввести `admin`  
2. Поле "Password" оставить пустым
3. Нажать кнопку "Log In"  

*Ожидаемый результат*: В форме авторизации выводится сообщение `Enter Both Username And Password`, если хотя бы одно из полей не заполнено   
*Фактический результат*: В форме авторизации выводится сообщение `Please Enter Bouth Username And Password`, если хотя бы одно из полей не заполнено   
*Окружение*: Google Chrome, Версия 113.0.5672.93

## Баг №4 Орфографическая ошибка в слове "Bouth" в тексте сообщения при незаполнении одного из полей при авторизации 
*Предшествующие условия*:  
1. Открыто окно авторизации http://testingchallenges.thetestingmap.org/login/login.php    

*Серьезность* : **Trivial**  
*Шаги воспроизведения*: 
1. В поле "Username" ввести `admin`  
2. Поле "Password" оставить пустым
3. Нажать кнопку "Log In"   

*Ожидаемый результат*: В форме авторизации выводится сообщение `Enter Both Username And Password`, если хотя бы одно из полей не заполнено    
*Фактический результат*: Если хотя бы одно из полей не заполнено, в форме авторизации выводится сообщение `Please Enter Bouth Username And Password`, в котором допущена орфографическая ошибка в слове Bouth --> Both  
*Окружение*: Google Chrome, Версия 113.0.5672.93  

## Баг №5 При вводе в поле Username любого значения, кроме admin, появляется некорректное сообщение системы  
*Предшествующие условия*:  
1. Открыто окно авторизации http://testingchallenges.thetestingmap.org/login/login.php    

*Серьезность* : **Minor**  
*Шаги воспроизведения*: 
1. В поле "Username" ввести `user` 
2. В поле "Password" ввести `admin`
3. Нажать кнопку "Log In"   

*Ожидаемый результат*: В форме авторизации выводится сообщение `Username Not Found`, если введено любое другое значение username, кроме admin    
*Фактический результат*: В форме авторизации выводится сообщение `Username Not Found Log In Successfull`, если введено любое другое значение username, кроме admin   
*Окружение*: Google Chrome, Версия 113.0.5672.93  

## Баг №6 При вводе верного логина и некорректного пароля выводится некорректная ошибка 
*Предшествующие условия*:  
1. Открыто окно авторизации http://testingchallenges.thetestingmap.org/login/login.php    

*Серьезность* : **Minor**  
*Шаги воспроизведения*: 
1. В поле "Username" ввести `user` 
2. В поле "Password" ввести `admin`  

*Ожидаемый результат*: В форме авторизации выводится сообщение `Incorrect password`, если введён верный username и неверный пароль  
*Фактический результат*: В форме авторизации выводится сообщение `Username Not Found Log In Successfull`, если введён верный username и неверный пароль  
*Окружение*: Google Chrome, Версия 113.0.5672.93  

## Баг №7 Сообщения системы об ошибке отображаются в левом верхнем углу экрана
*Предшествующие условия*:  
1. Открыто окно авторизации http://testingchallenges.thetestingmap.org/login/login.php    

*Серьезность* : **Trivial**  
*Шаги воспроизведения*:  
1. В поле "Username" ввести `admin`  
2. Поле "Password" оставить пустым
3. Нажать кнопку "Log In"  

*Ожидаемый результат*: В результате любых действий, приводящих к ошибке (пример с пустым полем "Password"), сообщения системы об ошибке располагаются над кнопкой "Log in"    
*Фактический результат*: В результате действий, приводящих к ошибке, сообщения системы об ошибке располагаются в левом верхнем углу экрана  
*Окружение*: Google Chrome, Версия 113.0.5672.93  

## Баг №8 При попытке авторизации выбранный язык French сбрасывается на English 
*Предшествующие условия*:  
1. Открыто окно авторизации http://testingchallenges.thetestingmap.org/login/login.php    

*Серьезность*: **Critical**  
*Шаги воспроизведения*:  
1. Выбрать язык окна авторизации - "French"
1. В поле "Nom D'utilisateur" ввести `admin`  
2. Поле "Mot De Passe" ввести `admin`
3. Нажать кнопку "S'identifier"  
 
*Ожидаемый результат*: Выбранный язык French не сбрасывается после попытки авторизации  
*Фактический результат*: Выбранный язык French сбрасывается после попытки авторизации на English  
*Окружение*: Google Chrome, Версия 113.0.5672.93  

## Баг №9 При попытке авторизации выбранный язык Italian сбрасывается на English   
*Предшествующие условия*:  
1. Открыто окно авторизации http://testingchallenges.thetestingmap.org/login/login.php    

*Серьезность*: **Critical**   
*Шаги воспроизведения*:  
1. Выбрать язык окна авторизации - "Italian"
1. В поле "Nome Utente" ввести `admin`  
2. Поле "Parola D'ordine" ввести `admin`
3. Нажать кнопку "S'identifier"  
 
*Ожидаемый результат*: Выбранный язык Italian не сбрасывается после попытки авторизации  
*Фактический результат*: Выбранный язык Italian сбрасывается после попытки авторизации на English   
*Окружение*: Google Chrome, Версия 113.0.5672.93  

## Баг №10 При попытке авторизации выбранный язык German сбрасывается на English 
*Предшествующие условия*:  
1. Открыто окно авторизации http://testingchallenges.thetestingmap.org/login/login.php    

*Серьезность*: **Critical**    
*Шаги воспроизведения*:  
1. Выбрать язык окна авторизации - "German"
1. В поле "Benutzername" ввести `admin`  
2. Поле "Passwort" ввести `admin`
3. Нажать кнопку "S'identifier"  
 
*Ожидаемый результат*: Выбранный язык German не сбрасывается после попытки авторизации  
*Фактический результат*: Выбранный язык German сбрасывается после попытки авторизации на English    
*Окружение*: Google Chrome, Версия 113.0.5672.93  

## Баг №11 При выборе языка German кнопка Log In отображается на французском языке 
*Предшествующие условия*:  
1. Открыто окно авторизации http://testingchallenges.thetestingmap.org/login/login.php    

*Серьезность*: **Major**   
*Шаги воспроизведения*:  
1. Выбрать язык окна авторизации - "German"
 
*Ожидаемый результат*: Весь текст в окне авторизации будет на немецком языке (EINLOGGEN MELDEN SIE SICH DETAILS / Benutzername / Passwort / **Anmelden**)  
*Фактический результат*: Текст в окне авторизации не весь на выбранном немецком языке, кнопка Log In отображается на французском (EINLOGGEN MELDEN SIE SICH DETAILS / Benutzername / Passwort / **S'identifier**)     
*Окружение*: Google Chrome, Версия 113.0.5672.93  

## Баг №12 При выборе языка Italian кнопка Log In отображается на французском языке 
*Предшествующие условия*:  
1. Открыто окно авторизации http://testingchallenges.thetestingmap.org/login/login.php    

*Серьезность*: **Major**   
*Шаги воспроизведения*:  
1. Выбрать язык окна авторизации - "Italian"
 
*Ожидаемый результат*: Весь текст в окне авторизации будет на итальянском языке (INGRESSO ENTRA NEL DETTAGLIO / Nome Utente/ Parola D'ordine / **Accedi**)  
*Фактический результат*: Текст в окне авторизации не весь на выбранном итальянском языке, кнопка Log In отображается на французском (INGRESSO ENTRA NEL DETTAGLIO / Nome Utente/ Parola D'ordine / **S'identifier**)     
*Окружение*: Google Chrome, Версия 113.0.5672.93   

## Баг №13 В форме авторизации не выводится сообщение об ошибке, если не заполнены оба поля: логин и пароль 
*Предшествующие условия*:  
1. Открыто окно авторизации http://testingchallenges.thetestingmap.org/login/login.php    

*Серьезность*: **Minor**   
*Шаги воспроизведения*:  
1. Поле "Username" оставить пустым   
2. Поле "Password" оставить пустым
3. Нажать кнопку "Log In"
 
*Ожидаемый результат*: В форме авторизации выводится сообщение об ошибке, если не заполнены оба поля: логин и пароль  
*Фактический результат*: В форме авторизации не выводится сообщение об ошибке, если не заполнены оба поля: логин и пароль     
*Окружение*: Google Chrome, Версия 113.0.5672.93  